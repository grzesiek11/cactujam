#pragma once

#include "Entity.hpp"

class Finish : public Entity {
    
    public:
        virtual std::string entityType();

        Finish();

        virtual void tick(std::vector<Entity*>);

    private:
        //
    
};