#include "Player.hpp"
#include "AssetManager.hpp"
#include "Monster.hpp"
#include "globals.hpp"

std::string Player::entityType() {
    return "Player";
}

Player::Player() {
    winFlag = nullptr;

    x = 0;
    moveDirection = MoveDirection::None;
    jumpTimer = 0;
    attackTimer = 0;
    moving = 0;

    sprite.setTexture(*AssetManager::getTexture("grzesiek"));
    sprite.setScale(PLAYER_SIZE, PLAYER_SIZE);
    sprite.setPosition(INITIAL_X, INITIAL_Y);

    bagSprite.setTexture(*AssetManager::getTexture("bag_0"));
    bagSprite.setScale(PLAYER_SIZE, PLAYER_SIZE);
    sand = STARTING_SAND;
    leakFraction = 0.00;
}

Player::Player(bool *winFlag) : Player::Player() {
    this->winFlag = winFlag;
}

#ifndef NDEBUG
void Player::setLeakage(double leakFraction) {
    this->leakFraction = leakFraction;
}
#endif

const sf::Sprite Player::getBagSprite() {
    return bagSprite;
}

const double Player::getLeakage() {
    return leakFraction;
}

const int Player::getLeftSand() {
    return sand;
}

void Player::tick(std::vector<Entity*> entities) {
    // Logic

    bool isInAir = inAir(findEntity("Ground", entities)->getSprite());

    // Leak the sand
    if (grainDropTimer.getElapsedTime().asMilliseconds() > 10) {
        sand -= leakFraction * sand / 50;
        grainDropTimer.restart();
    }

    if (findEntity("Finish", entities)->getSprite().getGlobalBounds().intersects(sprite.getGlobalBounds())) {
        *winFlag = true;
    }
    
    // Jumping and falling
    if (jumpTimer > 0) {
        sprite.move(0.0, - JUMP_SPEED);
        --jumpTimer;
    } else if (isInAir) {
        sprite.move(0.0, GRAVITY_SPEED);
    }

    // Attack
    if (attackTimer > 0) {
        Monster *monster = dynamic_cast<Monster*>(findEntity("Monster", entities));
        if (monster->getSprite().getGlobalBounds().intersects(sprite.getGlobalBounds())) {
            monster->damage(1);
            attackTimer = 0;
        }
        --attackTimer;
    }

    // Moving around
    if (moveDirection != MoveDirection::None) {
        // "Animation"
        if (moving % MOVING_ROTATION_TICKS == 0) {
            if (moving % (MOVING_ROTATION_TICKS * 2) == 0) {
                sprite.setRotation(- MOVING_ROTATION);
                bagSprite.setRotation(- MOVING_ROTATION);
            } else {
                sprite.setRotation(MOVING_ROTATION);
                bagSprite.setRotation(MOVING_ROTATION);
            }
        }

        if (moveDirection == MoveDirection::Right) {
            x += X_STEP;
        } else if (x > 0) {
            x -= X_STEP;
        }
        moveDirection = MoveDirection::None;
        ++moving;
    } else {
        moving = 0;
    }

    // Sprite & texture updates

    // Moving "animation" reset
    if (moving == 0 || isInAir) {
        sprite.setRotation(0.0);
        bagSprite.setRotation(0.0);
    }

    // Jump animation set/reset
    // TODO: Group and optimite the jump animation and moving "animation" code
    sf::Texture *jumpTexture = AssetManager::getTexture("grzesiek_jump");
    sf::Texture *normalTexture = AssetManager::getTexture("grzesiek");
    sf::Texture *attackTexture = AssetManager::getTexture("grzesiek_attack");
    if (attackTimer > 0) {
        sprite.setTexture(*attackTexture);
    } else if ((isInAir || jumpTimer > 0) && sprite.getTexture() != jumpTexture) {
        sprite.setTexture(*jumpTexture);
    } else if (!(isInAir || jumpTimer > 0) && sprite.getTexture() != normalTexture) {
        sprite.setTexture(*normalTexture);
    }

    // Change bag texture
    int leakPercent = leakFraction * 100;
    if ((int) (leakPercent * 100) % 10 == 0 && leakPercent <= 100 && leakPercent > 0) {
        bagSprite.setTexture(*AssetManager::getTexture("bag_" + std::to_string(leakPercent / 10)));
    }

    // Set bag relative to the player
    bagSprite.setPosition(sprite.getPosition().x - PLAYER_SIZE * 40, sprite.getPosition().y + PLAYER_SIZE * 500);
}

void Player::move(const MoveDirection moveDirection) {
    this->moveDirection = moveDirection;
}

void Player::jump(std::vector<Entity*> entities) {
    if (!inAir(findEntity("Ground", entities)->getSprite())) {
        jumpTimer = JUMP_TIMEOUT;
    }
}

void Player::attack() {
    if (attackTimer <= 0) {
        attackTimer = 10;
    }
}

void Player::punchBag(double damageFraction) {
    if (leakFraction + damageFraction < 1.00) {
        leakFraction += damageFraction;
    } else {
        leakFraction = 1.0;
    }
}

bool Player::inAir(const sf::Sprite ground) {
    return sprite.getPosition().y + sprite.getGlobalBounds().height < ground.getPosition().y + LOWER_BACKGROUND;
}