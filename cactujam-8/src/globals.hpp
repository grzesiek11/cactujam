#pragma once

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 800
#define LOWER_GROUND 50
#define LOWER_BACKGROUND 37
#define NAME "Idzie Grzesiek przez wies"
#define TPS 80
#define PLAYER_SIZE 0.30
#define INITIAL_X 50.0
#define INITIAL_Y 100.0
#define JUMP_TIMEOUT 20
#define MOVING_ROTATION 5.0
#define MOVING_ROTATION_TICKS 20
#define X_STEP 5.0
#define GRAVITY_SPEED 5.5
#define JUMP_SPEED 6.5
#define STARTING_SAND 1000000

#include "Entity.hpp"
#include <SFML/Graphics.hpp>

Entity *findEntity(std::string, std::vector<Entity*>);
std::vector<Entity*> findEntities(std::string, std::vector<Entity*>);
