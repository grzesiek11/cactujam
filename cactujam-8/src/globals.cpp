#include "globals.hpp"

Entity *findEntity(std::string type, std::vector<Entity*> entities) {
    for (Entity* entity: entities) {
        if (entity->entityType() == type) {
            return entity;
        }
    }
    return NULL;
}

std::vector<Entity*> findEntities(std::string type, std::vector<Entity*> entities) {
    for (auto itr = entities.begin(); itr != entities.end(); ++itr) {
        if ((*itr)->entityType() != type) {
            entities.erase(itr);
        }
    }
    return entities;
}