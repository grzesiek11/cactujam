# Spider Evolution (CactuJam 7)

## Polski

[**Kod źródłowy**](https://gitlab.com/grzesiek11/cactujam/-/tree/master/cactujam-7)

### Kompilacja

#### Linux

1. Pobierz repo wraz z submodułem.
```sh
git clone --recursive https://gitlab.com/grzesiek11/cactujam
cd cactujam
```
2. Zainstaluj SFML, g++ oraz make.
```sh
sudo apt install g++ make libsfml-dev # Użyj swojego menedżera pakietów
```
3. Wejdź do folderu `cactujam-7` i skompiluj grę.
```sh
cd cactujam-7
make release # Pomiń 'release' jeśli nie chcesz kompilować wersji zoptymalizowanej
```

### Użycie skompilowanej wersji

#### Linux

Dostępna jest tylko wersja 64-bit (jeśli używasz innej architektury, kompilacja nie powinna być większym problemem).

W folderze z grą:
```sh
LD_LIBRARY_PATH=. ./spiders
```

Jeśli masz zainstalowanego SFMLa, samo `./spiders` lub kliknięcie w plik `spiders` powinno działać.

#### Windows

Są dostępne wersje 64-bit i 32-bit, ale tylko wersja 64-bit działa poprawnie.

W folderze z grą kliknij dwukrotnie na plik `spiders.exe` lub `spiders32.exe`, w zależności od architektury.

### Instrukcje

- Otwieraj paczki przez klikanie w nie, aby otrzymać pająki.
- Pojawiają się one losowo od 3 do 5 sekund.
- Przeciągaj na siebie pająki, aby ewoluowały.
- Możesz mieć na raz maksymalnie 50 pająków (w tym 10 pudełek) na planszy.
- Pająki poruszają się losowo od 1 do 5 sekund. Za każdym razem dają ci zarobek w postaci kwadratu stadium, w którym się znajdują.
- Klikanie na pająka daje taki efekt jak jego samowolne przesuwanie się.
- Jest 12 stadiów ewolucji. Zdobycie ostatniego to cel gry.

### O grze

Jest to moja pierwsza gra z grafiką (zawsze robię gry w terminalu). SFMLa, czyli libki której użyłem, uczyłem się w trakcie jamu :P Jest to również moja pierwsza ukończona gra na CactuJam.

Pomysł podpatrzyłem od gry na Androida o nazwie "Cow Evolution" (grałem w nią kilka dni w 4 czy 5 klasie podstawówki). Miałem zamiar zrobić taką grę na pierwszego CactuJama, ale najwidoczniej byłem wtedy sporo słabszym programistą, bo nie udało mi się dojść do etapu przeciągania pająków.

Ta gra jest nudna, nie mogę powiedzieć inaczej. Byłaby dużo ciekawsza (jak dla mnie przynajmniej), gdyby udało mi się zrobić mechanikę sklepu (jedyne co zrobiłem to pieniądze, które nic nie dają).

Osobiście podoba mi się pomysł ze zrobieniem stylistyki zeszytu w kratkę. Wymyślanie, a później rysowanie różniastych pająków było bardzo przyjemne.

Możecie się spodziewać ulepszonego asset flipa na kolejnym jamie :D

## English

[**Source code**](https://gitlab.com/grzesiek11/cactujam/-/tree/master/cactujam-7)

### Compiling

#### Linux

1. Download the repo with submodules.
```sh
git clone --recursive https://gitlab.com/grzesiek11/cactujam
cd cactujam
```
2. Install SFML, g++ and make.
```sh
sudo apt install g++ make libsfml-dev # Use your package manager of choice.
```
3. Go into `cactujam-7` and compile.
```sh
cd cactujam-7
make release # Ommit 'release' if you don't want an optimized version.
```

### Running a binary

#### Linux

Only 64-bit version is avaliable. If you run a different architecture, compile from source.

In game directory:
```sh
LD_LIBRARY_PATH=. ./spiders
```

If you have SFML avaliable, just `./spiders` or doubleclicking `spiders` should work.

#### Windows

There are 64bit and 32bit versions, but only the first one works well.

In game directory doubleclick `spiders.exe` or `spiders32.exe` depending on the version.

### How to play

- Open boxes by clicking on them to get spiders.
- They appear randomly every 3-5 seconds.
- Drag spiders on each other to evolve them.
- You can have max. 50 spiders (including 10 boxes) at one time.
- Spiders move randomly every 1-5 second(s). By moving, they earn you the stage they are in squared.
- Clicking on spiders gives the same effect that them moving does.
- There are 12 evolution stages. Getting the last one is the goal of the game.

### About

I've got the idea from the Android game "Cow Evolution" (I played it a bit while in elementary school). I wanted to make something like this since the 1st CactuJam, but I were too bad of a programmer to make it to the "moving spiders around" past then.

This game is kinda boring, I have to admit. It wouldn't be that much (at least for me) if I had implemented the "shop" mechanic (only thing I did is useless money).

I like the idea of the "notebook" style I went for. Drawing the different spiders was a positive experience :)

You can expect more polished asset-flip on the next CactuJam :D
